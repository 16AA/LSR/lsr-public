#define COMPONENT patch_contact_in_cup_c
#include "\z\LSR\addons\main\script_mod.hpp"

// #define DEBUG_MODE_FULL
// #define DISABLE_COMPILE_CACHE

#ifdef DEBUG_ENABLED_PATCH_CONTACT_IN_CUP_C
    #define DEBUG_MODE_FULL
#endif
    #ifdef DEBUG_SETTINGS_PATCH_CONTACT_IN_CUP_C
    #define DEBUG_SETTINGS DEBUG_SETTINGS_PATCH_CONTACT_IN_CUP_C
#endif

#include "\z\LSR\addons\main\script_macros.hpp"
