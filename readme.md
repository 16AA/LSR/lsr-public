# The 16AA's public mods

This repository is the home of the [16AA](https://16aa.net)'s public modifications for ArmA 3. Contributions are very welcome.

We have a large number of private mods we run for ourselves. We'd like to make as many of these public as possible, but it might take a while.

We try to make everything as modular as possible. If you don't want a PBO, just delete it.

# Installing

[Download the latest release here](https://github.com/16AAModTeam/LSR-public/releases/latest/download/@16aa_public.zip) or subscribe on the [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1883956552). Extract and run @16aa_public/ as you would any other ArmA modification.

Requirements: CUP.

# What does it do?

- Replace CUP buildings with their equivalents from the Livonia terrain - i.e. more buildings with interiors on Chernarus, etc. We've tried to do it tastefully as you can see in the screenshots below.

Before (approx two buildings with interiors):

![Before](https://i.imgur.com/Ocl7xEt.jpg)

After (approx thirty buildings with interiors): 

![After](https://i.imgur.com/Ur5eAfW.jpg)

# Building (developers only)

1. Install [hemtt](https://github.com/synixebrett/HEMTT/releases) on your PATH (if you're unsure, you probably want ...x86_65-pc-windows-msvc). If putting it on your PATH sounds complicated, just put it in the same directory as this repository.
2. Clone this repository
3. Run `hemtt build`
4. Add this folder as a mod to ArmA 3 as you would normally.
5. Start ArmA 3!
